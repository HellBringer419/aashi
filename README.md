# Aashi

An attempt to make a YouTube alternative. WORK IN PROGRESS.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Usage

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.


## Ideology

Let's recreate YouTube - how would we build a centralized video sharing platform. (not being affliated to any company )
I'm thinking of building a server (in Spring) that would be served up in Linode and have the actual content ... videos and all.
Then, the Next app will have the UI in client and various algorithms (class P types; things you don't need training for) in the server.
Have a base free, guess from my pocket, but then increased playtime would cost so I can pay up the server costs and a way to pay on the creators. For the creator part; think of it as a built in patreon. 
At this moment, this seems like a good idea. Now, we build.

### Reasoning

Man, you can't use YouTube any other account except google. Let's change that.


## Present Project Status
[ Dev At Work ]