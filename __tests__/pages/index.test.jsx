import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import Home from '../../pages/index';

describe('Home', () => {
  it('renders a heading', () => {
    render(<Home />);

    const heading = screen.getByRole('heading', {
      name: /Aashi/,
    });

    expect(heading).toBeInTheDocument();
  });

  it('renders a sub-heading', () => {
    render(<Home />);

    const subheading = screen.getByRole('heading', {
      name: /YouTube alternative/,
    });

    expect(subheading).toBeInTheDocument();
  });
})
