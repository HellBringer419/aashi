import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import Player from '../../pages/player';


describe('Player', () => {
  it('exists', async () => {
    render(<Player />);

    const player = await screen.findByLabelText(/Video Player/i);
    expect(player).toBeInTheDocument();
  })
}) 