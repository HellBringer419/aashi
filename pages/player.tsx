import type { NextPage } from 'next'

const Player: NextPage = () => {
  return (
    <video
      id="my-player"
      className="video-js"
      controls
      preload="auto"
      autoPlay={false}
      data-setup="{}"
      aria-label="A video player"
    >
      <source
        src="https://assets.mixkit.co/videos/preview/mixkit-conceptual-shot-of-silhouettes-2369-large.mp4"
        type="video/mp4"
      />
    </video>
  )
}

export default Player
